import { AppContext } from "./app-context"
import { setPage } from "./main";
import { UserObject } from "./types/user-profile";

export default function LandingScreen() {
    const logout = () => {
        AppContext.userObject = null as unknown as UserObject;
        window.localStorage.removeItem("token");
        setPage("login");
    }

    return <section id="profile" className="profile-section">
    <button onClick={logout}>Logout</button>
    <button onClick={()=>setPage("music-library")}>Go to library</button>
    <h2>Logged in as <span id="displayName"></span></h2>
    <div className="profile-content">
      <span id="avatar"></span>
      <ul>
        <li><b>User ID:</b> <span id="id">{AppContext.userObject.id}</span></li>
        <li><b>Spotify URI:</b> <a id="uri" href={AppContext.userObject.spotifyUri}>Click here</a></li>
      </ul>
      <div className="user-avatar" style={{ backgroundImage: `url(${AppContext.userObject.profileImage})` }}></div>
    </div>
  </section> 
}