export interface ExtendedSongInfo {
    name: string;
    popularity: number;
    duration_ms: number;
    explicit: boolean;
    id: string;
    href: string;
    album: {
        name: string;
        genres: string[],
        images: {
            url: string
        }[]
    }
}