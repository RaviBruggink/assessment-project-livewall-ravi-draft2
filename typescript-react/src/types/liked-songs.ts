/**
 * Represents a liked song.
 */
export interface LikedSong {
  album: string;
  albumImage: string;
  artists: string;
  id: string;
  name: string;
  uri: string;
}