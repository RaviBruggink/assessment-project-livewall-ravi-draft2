/**
 * The spotify user object.
 */
export interface UserObject {
    id: string;
    spotifyUri: string;
    profileImage: string;
  }