import { AppContext } from "./app-context"
import { setPage } from "./main";
import "./music-library.css";
import { ExtendedSongInfo } from "./types/ext-song-info";
import { useState } from "react";
import axios from "axios";

interface SongDialogState {
    visible: boolean;
    song: ExtendedSongInfo;
}

export default function MusicLibraryScreen() {
    const [dialogState, setDialogState] = useState<SongDialogState>({ visible: false, song: null as unknown as ExtendedSongInfo });

    /**
     * Gets the song information.
     */
    async function getSongInfo(song_id: string) {
        const token = localStorage.getItem('token');

        try {
            const response = await axios.get(`https://api.spotify.com/v1/tracks/${song_id.substring(14)}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });

            const songInfo = response.data as ExtendedSongInfo;
            setDialogState({ visible: true, song: songInfo });
        } catch(e) {
            alert("Failed to get song info!");
        }
    }

    const closeDialog = ()=>setDialogState({ visible: false, song: null as unknown as ExtendedSongInfo });

    // Render the music library
    // Map the liked songs into JSX elements so we can put it in a table
    return <div className="music-library">
        <div className="list-container">
            <div className="actions">
                <div className="action back" onClick={()=>setPage("landing")}>⬅ Back to home</div>
            </div>
            <div className="contents">
                <h2>Liked Songs</h2>
                <table className="songs">
                    <tbody>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Song Name</th>
                            <th>Song Artists</th>
                        </tr>
                        { AppContext.likedSongs.map(x => <tr className="song" key={x.id}>
                            <td><img src={x.albumImage}/></td>
                            <td className="name" onClick={()=>getSongInfo(x.uri)}>{x.name}</td>
                            <td>{x.artists}</td>
                        </tr>) }
                    </tbody>
                </table>    
            </div>
        </div>

        {
                dialogState.visible ?
        <div className="dlg-container">
            <div className="dialog song-info">
                    <div className="titlebar">
                        <div className="title">Song Information</div>
                        <button className="close" onClick={()=>closeDialog()}>x</button>
                    </div>
                    <div className="body">
                        <div className="cover">
                            <img src={dialogState.song.album.images[0].url}/>
                        </div>
                        <div className="fields">
                            <div className="field"><b>Name: </b>{dialogState.song.name}</div>
                            <div className="field"><b>Album: </b>{dialogState.song.album.name}</div>
                            <div className="field"><b>Duration: </b>{dialogState.song.duration_ms / 1000} seconds</div>
                            <div className="field"><b>Explicit: </b>{dialogState.song.explicit ? 'Yes' : 'No'}</div>
                        </div>
                    </div>
                    <div className="buttons">
                        <button className="spotify" onClick={()=>window.open(`https://open.spotify.com/track/${dialogState.song.id}`)}>OPEN IN SPOTIFY</button>
                        <button className="ok" onClick={()=>closeDialog()}>OK</button>
                    </div>
                </div>
        </div>
        : ''
        }
    </div>
}