import React, { useState } from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { AppContext } from './app-context.tsx'

interface LayoutChangerState {
  pageName: string;
}

let currentPageSetter: (arg0: { pageName: string; }) => void;

function LayoutComponent() {
  const [currentPage, setCurrentPage] = useState<LayoutChangerState>({ pageName: "login" });
  currentPageSetter = setCurrentPage;

  return <div className='layout-mgr'>
    { AppContext.getCurrentUIPage(currentPage.pageName) }
  </div>
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <LayoutComponent/>
  </React.StrictMode>,
)

/**
 * Sets the page to display.
 */
export function setPage(name: string) {
  currentPageSetter({ pageName: name });
}