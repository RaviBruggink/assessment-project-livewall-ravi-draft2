import { ReactElement } from "react";
import LoginScreen from "./Login";
import { UserObject } from "./types/user-profile"
import LandingScreen from "./landing";
import MusicLibraryScreen from "./music-library";
import { LikedSong } from "./types/liked-songs";
/**
 * Represents the current app context.
 */
export class AppContext {
    /**
     * The spotify user that's currently logged in.
     */
    static userObject: UserObject;
    /**
     * The user's liked songs.
     */
    static likedSongs: LikedSong[];

    /**
     * Gets the current UI page.
     */
    static getCurrentUIPage(pageName: string) : React.ReactElement | null {
        switch(pageName) {
            case "login":
                return <LoginScreen/>;
            case "landing":
                return <LandingScreen/>;
            case "music-library":
                return <MusicLibraryScreen/>;
        }

        return null;
    }
}