import { useEffect, useState } from 'react';
import axios from 'axios';

import './Login.css'; // import your CSS file
import { UserObject } from './types/user-profile';
import { AppContext } from './app-context';
import { setPage } from './main';

function LoginScreen() {
  // Application client id
  const CLIENT_ID = "d2430508e1874b46b3fdef828468307b";

  // Redirect URI (this app)
  const REDIRECT_URI = "http://localhost:5173/";

  // Scopes (our permissions)
  const SCOPES = ["user-library-read"];
  let fmtScopes = "";

  // Create query string for scopes
  for(let item of SCOPES) {
    fmtScopes += item + "%20";
  }

  const AUTH_ENDPOINT = "https://accounts.spotify.com/authorize";
  const RESPONSE_TYPE = "token";

  const [token, setToken] = useState<string | null>(null);
  const [userObject, setUserObject] = useState<UserObject>({ id: "", spotifyUri: "", profileImage: "" });

  // Get the token and authorize.
  useEffect(() => {
    const hash = window.location.hash;
    let token = window.localStorage.getItem("token");

    // getToken()

    if (!token && hash) {
      const token = hash
      .substring(1)
      .split("&")
      .find((elem) => elem.startsWith("access_token"))
      ?.split("=")[1];

      window.location.hash = "";
      if (token) {
        window.localStorage.setItem("token", token);
      }
    }
    //console.log(token);
    setToken(token);

    (async() => {
      if((!window.location.hash.includes("access_token")) || (localStorage.getItem("token") == null))
        return; // Do nothing

      // Step 1: Fetch liked songs for the client
      try {
        const response = await axios.get("https://api.spotify.com/v1/me/tracks", {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });
    
        // Format received data
        const likedSongs = response.data.items.map((item: any) => ({
          id: item.track.id,
          name: item.track.name,
          artists: item.track.artists.map((artist: any) => artist.name).join(", "),
          album: item.track.album.name,
          albumImage: item.track.album.images[0].url,
          uri: item.track.uri
        }));
    
        // do something with the liked songs data
        AppContext.likedSongs = likedSongs;
      } catch (error) {
        console.log(error);
      }

      // Step 2: Fetch user profile
      try {
        // Fetch user profile
        const response = await axios.get("https://api.spotify.com/v1/me", {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });

        // Get only the necessary user data
        let retrievedUserObj = {
          id: response.data.id,
          spotifyUri: response.data.external_urls.spotify || "",
          profileImage: response.data.images[0].url
        } as UserObject;

        // Set user object state and tell app context which user is logged in
        setUserObject(retrievedUserObj);
        AppContext.userObject = retrievedUserObj;

        // Load the landing page (main interface)
        setPage("landing");
      } catch(e) {
        console.log("Failed to get user profile:");
        console.error(e);
        throw e;
      }
    })();

  }, []);
  
  return (
    <div className='display-content'>
      <div className="login-container">
        <form>
            <a className='login-button' type="submit" href={`${AUTH_ENDPOINT}?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=${RESPONSE_TYPE}&scope=${fmtScopes.substring(0, fmtScopes.length - 3)}`}>Login using spotify</a>      
        </form>
      </div>
    </div>
  );
}

export default LoginScreen;
