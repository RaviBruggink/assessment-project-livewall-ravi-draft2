# assessment-project-livewall-ravi-draft2

This is a Spotify profile viewer made in React + TypeScript + Vite.

## Getting started

Ensure you have NodeJS installed. The code files are contained within `typescript-react`.

Then, run `npm install` to install all required packages.

When done, run `npm run dev` to start a development server.
